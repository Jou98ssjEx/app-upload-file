import { uploadFile } from "./http-provider";

const body = document.body;
let inputFile, img;
const fileHtml = () => {
    const html = `
        <h1 class="mt-4"> Upload File to Cloudinary</h1>
        <hr>
        <div class="mb-3">
            <input type="file" accept="image/png, image/jpg, imagejpge" class="form-control">
        </div>

        <label class="">Choose to imagen</label>
        <div class="float-end mb-3">
            <img id="IMG" class="img-thumbnail w-25" src="" alt="img-Api">
        </div>
    `;
    const div = document.createElement('div');
    div.innerHTML = html;
    body.append(div);

    inputFile = document.querySelector('input');

    img = document.querySelector('#IMG');
}

// Events
const events = () => {
    inputFile.addEventListener('change', (e) => {
        const file = e.target.files[0];
        console.log(file);
        uploadFile(file).then(r => {
            img.src = r
        });
    });
}

export const init = () => {
    fileHtml();
    events();
}