import { getChiste } from "./http-provider";

const body = document.body;
let btnAgg, olList;

const createChisteHtml = () => {
    const html = `
        <h1 class="mt-4">Chistes</h1>
        <hr>

        <button class="btn btn-outline-primary"> Add</button>

        <ol class="mt-3 list-group">
         
        </ol>
    `;

    const divChistes = document.createElement('div');

    divChistes.innerHTML = html;

    body.append(divChistes);
}

const events = () => {
    btnAgg = document.querySelector('button');
    olList = document.querySelector('ol');

    let c = 0;
    btnAgg.addEventListener('click', async() => {
        console.log('Nuevo Chiste');
        // c = c + 1;
        c++;
        btnAgg.disable = true;
        dawChiste(await getChiste(), c);
        btnAgg.disable = false;
    })
}

// Create li chiste
// { id, value, icon_url }
const dawChiste = (chiste, count) => {
    const olItem = document.createElement('li');

    olItem.innerHTML = `<b> ${count}. ID: </b>${ chiste.id} <b>DATA:</b> ${chiste.value}`;
    olItem.classList.add('list-group-item');

    let imgChiste = document.createElement('img');
    // console.log(imgChiste);
    imgChiste.classList.add('rounded');
    imgChiste.classList.add('float-end');
    imgChiste.src = chiste.icon_url;
    imgChiste.alt = 'img-api';

    olItem.append(imgChiste);

    olList.append(olItem);
}

export const init = () => {
    createChisteHtml();
    events();
}