// CLoudinari
const cloudPreset = 'ml_default';
const cloudUrl = 'https://api.cloudinary.com/v1_1/dgekgbspr/upload';

const uploadFile = async(file) => {

    const formData = new FormData();
    formData.append('upload_preset', cloudPreset);
    formData.append('file', file);

    try {
        const resp = await fetch(cloudUrl, {
            method: 'POST',
            body: formData
        });

        if (resp.ok) {
            const data = await resp.json();
            alert('File uploaded successfully');
            // console.log(data);
            return data.secure_url;
        } else {
            throw await resp.json();
        }
    } catch (error) {
        throw error
    }
}


export {
    uploadFile
}